<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class testCohortsRetentionAnalisys extends TestCase {
    /**
     * A basic unit test example.
     * @return void
     */
    public function testExample () {
        $this->assertTrue(true);
    }
    
    public function testYAxis()
    {
        $response = $this->json('GET', route('chart'));
        $response->assertJsonFragment([
            'data' => [100, 100, 100, 40, 36, 36, 36, 28]
        ]);
    }
    
    public function testExistenceWeeklyLines()
    {
        $response = $this->json('GET', route('chart'));
        $response->assertJsonFragment([
            "name"=> "2016-07-18",
        ]);
        $response->assertJsonFragment([
            "name"=> "2016-07-25",
        ]);
        $response->assertJsonFragment([
            "name"=> "2016-08-01",
        ]);
        $response->assertJsonFragment([
            "name"=> "2016-08-08",
        ]);
    }

    public function testCohortsRetentionAnalysisWithoutMiddleWare () {
        $response = $this->json('GET', route('chart'));
        $response->assertStatus(200);
    }
    
    public function testCohortsRetentionAnalysisResponse () {
        $response = $this->json('GET', route('chart'));
        $response->assertStatus(200);
        $response->assertJsonStructure(
            [
                "series" => [
                    '*' => [
                        "name",
                        "data",
                    ]
                ]
            ]
        );
        $response->assertJsonCount(4,
            "series"
        );
    }
}
