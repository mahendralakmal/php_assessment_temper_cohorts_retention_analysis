<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * Class featureTestCohortsRetentionAnalisys
 * @package Tests\Feature
 */
class featureTestCohortsRetentionAnalisys extends TestCase
{
    /**
     *
     */
    public function testCohortsRetentionAnalysis()
    {
        $response = $this->get('/CohortAnalysis');
        
        $response->assertStatus(200);
        $response->assertSee('Cohort Analyst');
    }
}
