# Cohort Retention Analysis

###Nginx Configuration file

Create "temper.d.conf" in nginx and bellow are the configurations.

```
server {

    listen 80;
    listen [::]:80;

    server_name temper.d;
    root /var/www/php_assessment_temper_cohorts_retention_analysis/public;
    index index.php index.html index.htm;

    location / {
         try_files $uri $uri/ /index.php$is_args$args;
    }

    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_pass php-upstream;
        fastcgi_index index.php;
        fastcgi_buffers 16 16k;
        fastcgi_buffer_size 32k;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        #fixes timeouts
        fastcgi_read_timeout 600;
        include fastcgi_params;
    }

    location ~ /\.ht {
        deny all;
    }

    location /.well-known/acme-challenge/ {
        root /var/www/letsencrypt/;
        log_not_found off;
    }
}
```
 
 
 ###Migrations
* php artisan migrate

###Swapping CSV file
* Call route "fileread" to swap csv and seed to db.

#
* Register a user and login with it.
* Click "view Chart".
 
![alt text](https://bitbucket.org/mahendralakmal/php_assessment_temper_cohorts_retention_analysis/src/master/Screen_Shot_2019-09-01_at_9.35.41_PM.png)

