<?php

namespace App\Http\Controllers;

use App\CohortsAnalyzer;
use App\Repository\CohortRetentionAnalysis\CohortRetentionAnalysisOutPut;
use App\Repository\CohortRetentionAnalysis\Cohorts;
use Illuminate\Http\Request;

/**
 * Class CohortAnalysisController
 * @package App\Http\Controllers
 */
class CohortAnalysisController extends Controller {
    /**
     * @return CohortRetentionAnalysisOutPut
     */
    public function generateCohortAnalysis () {
        $analysis = new CohortsAnalyzer(new Cohorts());
        $data =  $analysis->CohortsAnalysis(new CohortRetentionAnalysisOutPut());
        return $data;
    }
}
