<?php

namespace App\Http\Controllers;

use App\Repository\FileRead\FileReadRepository;
use Illuminate\Http\Request;

/**
 * Class FileReadController
 * @package App\Http\Controllers
 */
class FileReadController extends Controller
{
    /**
     * @var FileReadRepository
     */
    private $fileRead;

    /**
     * FileReadController constructor.
     * @param FileReadRepository $fileRead
     */
    public function __construct(FileReadRepository $fileRead)
    {
        $this->fileRead = $fileRead;
    }
    
    /**
     * @return mixed
     */
    public function getCSV(){
        return $this->fileRead->swappedCsvFile();
    }
}
