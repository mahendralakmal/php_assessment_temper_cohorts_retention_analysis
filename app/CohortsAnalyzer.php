<?php

namespace App;

use App\Repository\CohortRetentionAnalysis\CohortRetentionAnalysisOutPut;
use App\Repository\CohortRetentionAnalysis\CohortsRepository;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CohortsAnalyzer
 * @package App
 */
class CohortsAnalyzer extends Model {
    /**
     * @var CohortsRepository
     */
    private $repository;
    
    /**
     * CohortsAnalyzer constructor.
     * @param CohortsRepository $repository
     */
    public function __construct (CohortsRepository $repository) {
        $this->repository = $repository;
    }
    
    /**
     * @param $data
     * @return mixed
     */
    public function formDataArray ($data) {
        foreach ($data as $week) {
            $dataArr = array();
            for ($i = 1; $i <= 8; $i++) {
                if ($i == 1) {
                    $dataArr[] = 100;
                } else {
                    $dataArr[] = round(($week->{"Step" . $i} / $week->Step1) * 100);
                }
            }
            $chartArr ["series"] [] = array("name" => $week->week_start, "data" => $dataArr);
        }
        return $chartArr;
    }
    
    
    /**
     * @param CohortRetentionAnalysisOutPut $formateOutPut
     * @return CohortRetentionAnalysisOutPut|\Illuminate\Http\JsonResponse
     */
    public function CohortsAnalysis (CohortRetentionAnalysisOutPut $formateOutPut) {
        $dbData = $this->repository->cohortAnalysis();
        $chartArray = $this->formDataArray($dbData);
        return $formateOutPut->output($chartArray);
    }
}
