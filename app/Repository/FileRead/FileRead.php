<?php


namespace App\Repository\FileRead;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class FileRead implements FileReadRepository {
    
    /**
     * @return array
     */
    public function swappedCsvFile () {
        $path = base_path() . ('/public/export.csv');
        
        $header = NULL;
        $data = array();
        if (($handle = fopen($path, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, ';')) !== FALSE) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }
        
        DB::table('UserOnboardSteps')->insert($data);
        return $data;
    }
}
