<?php


namespace App\Repository\CohortRetentionAnalysis;


/**
 * Class CohortRetentionAnalysisOutPut
 * @package App\Repository\CohortRetentionAnalysis
 */
class CohortRetentionAnalysisOutPut implements CohortRetentionAnalysisOutPutRepository {
    
    /**
     * @param $retention
     * @return \Illuminate\Http\JsonResponse
     */
    public function output ($retention) {
        return response()->json($retention)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }
}
