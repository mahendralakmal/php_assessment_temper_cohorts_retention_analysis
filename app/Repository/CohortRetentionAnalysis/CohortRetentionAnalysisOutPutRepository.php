<?php


namespace App\Repository\CohortRetentionAnalysis;


/**
 * Interface CohortRetentionAnalysisOutPutRepository
 * @package App\Repository\CohortRetentionAnalysis
 */
interface CohortRetentionAnalysisOutPutRepository {
    /**
     * @param $retention
     * @return mixed
     */
    public function output ($retention);
}
