<?php


namespace App\Repository\CohortRetentionAnalysis;


/**
 * Interface CohortsRepository
 * @package App\Repository\CohortRetentionAnalysis
 */
interface CohortsRepository {
    /**
     * @return mixed
     */
    public function cohortAnalysis ();
}
